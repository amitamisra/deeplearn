'''
Created on Nov 15, 2016
Build an LSTM model from pretrained word embedding vectors. To save time, pretrained vectors are
stored in a file. 
@author: amita
'''
import numpy as np,os
import  file_utilities
import sys
from keras import callbacks
from keras.layers import Dense, LSTM, Dropout
from keras.models import Sequential

def  get_data_file_list(pos_file,neg_file):
    pos_file=os.path.join(file_utilities.DATA_DIR,pos_file)
    neg_file=os.path.join(file_utilities.DATA_DIR,pos_file)
    return(pos_file,neg_file)
    
class DataIterator:
    def __init__(self, data_path, batch_size = 1000):
        pos_files, neg_files = get_data_file_list(data_path)
        self.pos_iter = iter(pos_files)
        self.neg_iter = iter(neg_files)
        self.batchSize = batch_size

    def get_next(self):
        vectors = []
        values = []
        while (len(vectors) < self.batchSize):

            file = next (self.pos_iter, None)
            if file == None:
                break
            vec = np.load(self.data_path + file)
            vectors.append(vec)
            values.append([1,0])

            file = next(self.neg_iter, None)
            if file == None:
                break
            vec = np.load(self.data_path + file)
            vectors.append(vec)
            values.append([0,1])
        return np.array(vectors), np.array(values)


def train(train_data_path,test_data_path):
    timesteps = 50
    dimensions = 300
    batch_size = 64
    epochs_number = 40
    model = Sequential()
    model.add(LSTM(200,  input_shape=(timesteps, dimensions),  return_sequences=False))
    model.add(Dropout(0.2))
    model.add(Dense(2, input_dim=200, activation='softmax'))
    model.compile(loss="categorical_crossentropy", optimizer='rmsprop')
    fname = 'weights/keras-lstm.h5'
    model.load_weights(fname)
    cbks = [callbacks.ModelCheckpoint(filepath=fname, monitor='val_loss', save_best_only=True),
            callbacks.EarlyStopping(monitor='val_loss', patience=3)]
    train_iterator = DataIterator(train_data_path, sys.maxsize )
    test_iterator = DataIterator(test_data_path, sys.maxsize )
    train_X, train_Y = train_iterator.get_next()
    test_X, test_Y = test_iterator.get_next()
    model.fit(train_X, train_Y, batch_size=batch_size, callbacks=cbks, nb_epoch=epochs_number,
              show_accuracy=True, validation_split=0.25, shuffle=True)
    loss, acc = model.evaluate(test_X, test_Y, batch_size, show_accuracy=True)
    print('Test loss / test accuracy = {:.4f} / {:.4f}'.format(loss, acc))
if __name__ == '__main__':
    pass