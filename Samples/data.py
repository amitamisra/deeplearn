'''
Created on Oct 31, 2016

@author: amita
'''
import pandas as pd
def splifeatures_label(inputcsv,class_label,feature_list):
    df=pd.read_csv(inputcsv,na_filter=False)
    colnames=list(df.columns.values)
    feature_cols=[col for col in colnames if str(col).startswith(tuple(feature_list))]
    Y_values= df[class_label].values
    X_values= df[list(feature_cols)].values
    return(X_values,Y_values)

    
if __name__ == '__main__':
    pass